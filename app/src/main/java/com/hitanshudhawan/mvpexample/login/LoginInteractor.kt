package com.hitanshudhawan.mvpexample.login

interface LoginInteractor {

    fun login(username: String, onLoginFinishedListener: OnLoginFinishedListener)

    interface OnLoginFinishedListener {

        fun onSuccess()
        fun onUsernameError()
        fun onNetworkError()

    }

}