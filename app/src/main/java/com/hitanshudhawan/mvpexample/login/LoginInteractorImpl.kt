package com.hitanshudhawan.mvpexample.login

import com.hitanshudhawan.mvpexample.network.ApiClient
import com.hitanshudhawan.mvpexample.network.ApiInterface
import com.hitanshudhawan.mvpexample.network.UserDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginInteractorImpl : LoginInteractor {

    override fun login(username: String, onLoginFinishedListener: LoginInteractor.OnLoginFinishedListener) {

        val apiService: ApiInterface = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.getUserDetails(username).enqueue(object : Callback<UserDetails> {

            override fun onResponse(call: Call<UserDetails>?, response: Response<UserDetails>?) {

                when {
                    response?.isSuccessful!! -> onLoginFinishedListener.onSuccess()
                    response.code() == 404 -> onLoginFinishedListener.onUsernameError()
                    else -> onLoginFinishedListener.onNetworkError()
                }

            }

            override fun onFailure(call: Call<UserDetails>?, t: Throwable?) {
                onLoginFinishedListener.onNetworkError()
            }

        })

    }

}