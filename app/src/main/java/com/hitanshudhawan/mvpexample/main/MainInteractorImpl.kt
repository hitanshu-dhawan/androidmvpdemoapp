package com.hitanshudhawan.mvpexample.main

import com.hitanshudhawan.mvpexample.network.ApiClient
import com.hitanshudhawan.mvpexample.network.ApiInterface
import com.hitanshudhawan.mvpexample.network.UserDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainInteractorImpl : MainInteractor {

    override fun loadUserProfile(username: String, onLoadingFinishedListener: MainInteractor.OnLoadingFinishedListener) {

        val apiService: ApiInterface = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.getUserDetails(username).enqueue(object : Callback<UserDetails> {

            override fun onResponse(call: Call<UserDetails>?, response: Response<UserDetails>?) {

                if (response?.isSuccessful!!) {
                    onLoadingFinishedListener.onSuccess(response.body())
                } else {
                    onLoadingFinishedListener.onError()
                }

            }

            override fun onFailure(call: Call<UserDetails>?, t: Throwable?) {
                onLoadingFinishedListener.onError()
            }

        })

    }

}