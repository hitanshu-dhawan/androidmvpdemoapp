package com.hitanshudhawan.mvpexample.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.hitanshudhawan.mvpexample.R
import com.hitanshudhawan.mvpexample.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(this) }

    private val presenter: LoginPresenter = LoginPresenterImpl(this, LoginInteractorImpl())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)

        supportActionBar!!.title = "Login"

        loginButton.setOnClickListener {
            presenter.validateUsername(loginEditText.text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showProgressDialog() {
        progressDialog.setMessage("Checking your GitHub username...")
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun setUsernameError() {
        loginEditText.error = ("This username doesn't exist")
    }

    override fun navigateToMainActivity() {
        val intent: Intent = Intent(this, MainActivity::class.java)
        intent.putExtra("username", loginEditText.text.toString())
        startActivity(intent)
    }

    override fun showNetworkError() {
        Toast.makeText(this, "Network error", Toast.LENGTH_SHORT).show()
    }

}
