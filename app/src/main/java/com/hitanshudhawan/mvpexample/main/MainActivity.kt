package com.hitanshudhawan.mvpexample.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.hitanshudhawan.mvpexample.R
import com.hitanshudhawan.mvpexample.network.UserDetails
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), MainView {

    private val username: String by lazy { intent.getStringExtra("username") }

    private val presenter: MainPresenter = MainPresenterImpl(this, MainInteractorImpl())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportActionBar!!.title = username

        presenter.loadUserProfile(username)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showProgressBar() {
        mainProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        mainProgressBar.visibility = View.INVISIBLE
    }

    override fun showUserProfile(userDetails: UserDetails?) {

        mainLinearLayout.visibility = View.VISIBLE

        Glide.with(this).load(userDetails?.avatarUrl).into(mainUserImageView)
        mainUserNameTextView.text = userDetails?.name
        mainUserBioTextView.text = userDetails?.bio
        mainFollowersTextView.text = "Followers : " + userDetails?.followers
        mainFollowingTextView.text = "Following : " + userDetails?.following

    }

    override fun showError() {
        Toast.makeText(this, "There was some error", Toast.LENGTH_SHORT).show()
    }
}
