package com.hitanshudhawan.mvpexample.main

import com.hitanshudhawan.mvpexample.network.UserDetails

interface MainInteractor {

    fun loadUserProfile(username: String, onLoadingFinishedListener: OnLoadingFinishedListener)

    interface OnLoadingFinishedListener {

        fun onSuccess(userDetails: UserDetails?)
        fun onError()

    }

}