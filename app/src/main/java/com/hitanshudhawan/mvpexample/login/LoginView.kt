package com.hitanshudhawan.mvpexample.login

interface LoginView {

    fun showProgressDialog()
    fun hideProgressDialog()
    fun setUsernameError()
    fun navigateToMainActivity()
    fun showNetworkError()

}