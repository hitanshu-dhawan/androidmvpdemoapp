package com.hitanshudhawan.mvpexample.main

interface MainPresenter {

    fun loadUserProfile(username: String)
    fun onDestroy()

}