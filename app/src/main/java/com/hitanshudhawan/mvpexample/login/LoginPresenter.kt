package com.hitanshudhawan.mvpexample.login

interface LoginPresenter {

    fun validateUsername(username: String)
    fun onDestroy()

}