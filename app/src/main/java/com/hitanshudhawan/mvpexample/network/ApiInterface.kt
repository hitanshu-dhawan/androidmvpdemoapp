package com.hitanshudhawan.mvpexample.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("users/{username}")
    fun getUserDetails(@Path("username") username: String): Call<UserDetails>

}