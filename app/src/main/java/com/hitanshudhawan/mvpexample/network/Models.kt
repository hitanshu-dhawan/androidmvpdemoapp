package com.hitanshudhawan.mvpexample.network

import com.google.gson.annotations.SerializedName

data class UserDetails(
        @SerializedName("login")
        val login: String,
        @SerializedName("avatar_url")
        val avatarUrl: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("bio")
        val bio: String,
        @SerializedName("followers")
        val followers: Int,
        @SerializedName("following")
        val following: Int
)