package com.hitanshudhawan.mvpexample.main

import com.hitanshudhawan.mvpexample.network.UserDetails

class MainPresenterImpl(private var mainView: MainView?, private val mainInteractor: MainInteractor) : MainPresenter, MainInteractor.OnLoadingFinishedListener {

    override fun loadUserProfile(username: String) {
        mainView?.showProgressBar()
        mainInteractor.loadUserProfile(username, this)
    }

    override fun onDestroy() {
        mainView = null
    }

    override fun onSuccess(userDetails: UserDetails?) {
        mainView?.hideProgressBar()
        mainView?.showUserProfile(userDetails)
    }

    override fun onError() {
        mainView?.hideProgressBar()
        mainView?.showError()
    }

}