package com.hitanshudhawan.mvpexample.main

import com.hitanshudhawan.mvpexample.network.UserDetails

interface MainView {

    fun showProgressBar()
    fun hideProgressBar()
    fun showUserProfile(userDetails: UserDetails?)
    fun showError()

}