package com.hitanshudhawan.mvpexample.login

class LoginPresenterImpl(private var loginView: LoginView?, private val loginInteractor: LoginInteractor) : LoginPresenter, LoginInteractor.OnLoginFinishedListener {

    override fun validateUsername(username: String) {
        loginView?.showProgressDialog()
        loginInteractor.login(username, this)
    }

    override fun onDestroy() {
        loginView = null
    }

    override fun onSuccess() {
        loginView?.hideProgressDialog()
        loginView?.navigateToMainActivity()
    }

    override fun onUsernameError() {
        loginView?.hideProgressDialog()
        loginView?.setUsernameError()
    }

    override fun onNetworkError() {
        loginView?.hideProgressDialog()
        loginView?.showNetworkError()
    }

}